
const yargs = require('yargs')
const fs = require("fs");
const userData = require("./user.json");

yargs.command({
    command: 'add',
    describe: 'Add language',
    builder: {
        title: {
            describe: 'Name languages',
            demandOption: true,
            type: 'string'
        },
        level: {
            describe: 'Current level',
            demandOption: true,
            type: 'string'
        }
    },
    handler(argv) {
        userData.languages.push({title: argv.title, level: argv.level})
    }
})

yargs.command({
    command: 'remove',
    describe: 'Remove language',
    builder: {
        title: {
            describe: 'Name languages',
            demandOption: true,
            type: 'string'
        }
    },
    handler(argv) {
        for(let e in userData.languages){
            if(userData.languages[e].title === argv.title){
                delete userData.languages[e]
            }
        }
    }
})

yargs.command({
    command: 'list',
    describe: 'List of languages',
    handler() {
        console.log(userData.languages)
    }
})
yargs.parse()



function saveJSON(filename = '', json = '"'){
    return fs.writeFileSync(
        filename,
        JSON.stringify(json, null, 2))
}

saveJSON('user.json', userData)