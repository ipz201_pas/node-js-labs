const lodash = require('lodash')
let array = [1, 2, 3, 4, 5, 7]
let arrayTrash = [1, 0 , 1 , 'a' , 4 , false, 'bobo', 0]
console.log(lodash.chunk(array, 2)) // делет массив на под массивы за указаным размером
console.log(lodash.compact(arrayTrash)) // это метод удаляет с массива: false, null, 0, "",  undefined, NaN
console.log(lodash.concat(array, 2 , [5])) // соединяет массивы
console.log(lodash.difference(array, [1, 2, 5, 4, 6, 10])) // ищет различия между первым и вторим массивом заданным в качестве второго параметра метода
console.log(lodash.drop(array, 5)) // n = default = 1 , удаляет количество элементов в массиве начиная с первого